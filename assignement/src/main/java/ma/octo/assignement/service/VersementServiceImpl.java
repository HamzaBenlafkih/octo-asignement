package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import ma.octo.assignement.domain.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;

@Service
public class VersementServiceImpl implements VersementService {

	Logger LOGGER = LoggerFactory.getLogger(VirrementService.class);
    
	@Autowired
	private VersementRepository versementRepository;

	@Autowired
	private CompteService compteService;

	@Autowired
	private TransactionService transactionService;

	@Override
	public List<VersementDto> AllVersement() {
		List<Versement> allVersement = versementRepository.findAll();
		return VersementMapper.listVersementToListVersementDto(allVersement);
	}

	@Override
	public VersementDto MakeVersement(VersementDto versementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

		Compte compteBenificiare = compteService.findCompteByRib(versementDto.getRibBeneficiaire());
		verifierCompte(compteBenificiare);
		verifierMontant(versementDto.getMontantVirement());
		verifierMotif(versementDto.getMotifVersement());

		compteBenificiare.setSolde(compteBenificiare.getSolde().add(versementDto.getMontantVirement()));
		compteService.saveCompte(compteBenificiare);

		Versement versement = new Versement();
		versement.setDateExecution(versementDto.getDateExecution());
		versement.setCompteBeneficiaire(compteBenificiare);
		versement.setMotifVersement(versementDto.getMotifVersement());
		versement.setMontantVirement(versementDto.getMontantVirement());
		versementRepository.save(versement);

		transactionService.auditVersement("Versement depuis " + versementDto.getNom_prenom_emetteur() + " vers "
				+ versementDto.getRibBeneficiaire() + " d'un montant de "
				+ versementDto.getMontantVirement().toString());
		return VersementMapper.versementToVersementDto(versement);
	}

	private void verifierCompte(Compte compteBenificiare) throws CompteNonExistantException {
		if (compteBenificiare == null) {
			LOGGER.error("Compte bénéficiare non existant");
			throw new CompteNonExistantException("Compte bénéficiare non existant");
		}
	}

	private void verifierMontant(BigDecimal montant) throws TransactionException {
		if (montant.intValue() < Constants.MONTANT_MINIMAL_VERSEMENT) {
			LOGGER.error("Montant minimal de versement non atteint");
			throw new TransactionException("Montant minimal de versement non atteint");
		} else if (montant.intValue() > Constants.MONTANT_MAXIMAL_VERSEMENT) {
			LOGGER.error("Montant maximal de versement dépassé");
			throw new TransactionException("Montant maximal de versement dépassé");
		}
	}

	private void verifierMotif(String motif) throws TransactionException {
		if (motif.length() <= 0) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}
	}

}
