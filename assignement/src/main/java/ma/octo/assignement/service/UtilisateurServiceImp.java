package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
public class UtilisateurServiceImp implements UtilisateurService{
	
	@Autowired
    private UtilisateurRepository utilisateurRepository;

	@Override
	public List<UtilisateurDto> FindAllUtilisateurs() {
		List<Utilisateur> allUtilisateur=utilisateurRepository.findAll();
	    return UtilisateurMapper.listUtilisateurToListUtilisateurDto(allUtilisateur);
	}

}
