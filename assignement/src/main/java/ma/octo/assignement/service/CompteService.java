package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.domain.Compte;

public interface CompteService {

	   List<CompteDto> FindAllComptes();
	   Compte findCompteByNumero(String nroCompte);
	   Compte findCompteByRib(String ribCompte);
	   Compte saveCompte(Compte Compte);
}
