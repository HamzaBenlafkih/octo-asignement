package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.mapper.CompteMapper;

@Service
public class CompteServiceImpl implements CompteService{
	
	@Autowired
    private CompteRepository compteRepository;

	@Override
	public List<CompteDto> FindAllComptes() {
		List<Compte> comptes = compteRepository.findAll();	
	    return CompteMapper.listCompteToListCompteDto(comptes);
	}

	@Override
	public Compte findCompteByNumero(String nrCompte) {
		return compteRepository.findByNrCompte(nrCompte);
	}

	@Override
	public Compte findCompteByRib(String ribCompte) {
		return compteRepository.findByRib(ribCompte);
	}

	@Override
	public Compte saveCompte(Compte compte) {
		return compteRepository.save(compte);
	}

}
