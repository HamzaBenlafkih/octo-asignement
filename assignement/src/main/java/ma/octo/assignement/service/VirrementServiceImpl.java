package ma.octo.assignement.service;

import java.util.List;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.util.*;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;
import java.util.Date;

@Service
public class VirrementServiceImpl implements VirrementService{
	
	Logger LOGGER = LoggerFactory.getLogger(VirrementService.class);

	@Autowired
	private VirementRepository virementRepository;

	@Autowired
	private CompteService compteService;

	@Autowired
	private TransactionService transactionService;

	@Override
	public List<VirementDto> AllVirement() {
		List<Virement> allVirement = virementRepository.findAll();
		return VirementMapper.listVirementToListVirementDto(allVirement);
	}

	@Override
	public VirementDto MakeVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		// TODO Auto-generated method stub
		Compte compteEmetteur = compteService.findCompteByNumero(virementDto.getNrCompteEmetteur());
		Compte compteBenificiare = compteService.findCompteByNumero(virementDto.getNrCompteBeneficiaire());
		
		verifierCompte(compteEmetteur, compteBenificiare);

		verifierMontant(virementDto.getMontantVirement());

		verifierMotif(virementDto.getMotif());

		verifierSolde(compteEmetteur.getSolde(), virementDto.getMontantVirement());
		
		compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
		compteService.saveCompte(compteEmetteur);

		compteBenificiare.setSolde(compteBenificiare.getSolde().add(virementDto.getMontantVirement()));
		compteService.saveCompte(compteBenificiare);
		
		Virement virement = new Virement();
        virement.setDateExecution(new Date());
        virement.setCompteBeneficiaire(compteBenificiare);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());
        virementRepository.save(virement);
        
        transactionService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers "
				+ virementDto.getNrCompteBeneficiaire() + " d'un montant de "
				+ virementDto.getMontantVirement().toString());

		return VirementMapper.virementToVirementDto(virement);
	}
	
	
	// Data validation methods
	
	private void verifierCompte(Compte compteEmetteur, Compte compteBenificiare) throws CompteNonExistantException {
		if (compteEmetteur == null) {
			LOGGER.error("Compte emetteur non existant");
			throw new CompteNonExistantException("Compte emetteur non existant");
		}
		if (compteBenificiare == null) {
			LOGGER.error("Compte bénéficiare non existant");
			throw new CompteNonExistantException("Compte bénéficiare non existant");
		}
	}

	private void verifierMontant(BigDecimal montant) throws TransactionException {
		if (montant.intValue() < Constants.MONTANT_MINIMAL_VIREMENT) {
			LOGGER.error("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");
		} else if (montant.intValue() > Constants.MONTANT_MAXIMAL_VIREMENT) {
			LOGGER.error("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");
		}
	}

	private void verifierMotif(String motif) throws TransactionException {
		if (motif.length() <= 0) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}
	}

	private void verifierSolde(BigDecimal soldeActuel, BigDecimal montant) throws SoldeDisponibleInsuffisantException {
		if (soldeActuel.intValue() < montant.intValue()) {
			LOGGER.error("Solde insuffisant pour l'emmeteur");
			throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'emmeteur");
		}

	}

}
