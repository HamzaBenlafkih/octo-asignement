package ma.octo.assignement.mapper;


import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.domain.Versement;

public class VersementMapper {
	
	public static VersementDto versementToVersementDto(Versement versement) {

		VersementDto versementDto = new VersementDto();
		versementDto.setMotifVersement(versement.getMotifVersement());
		versementDto.setMontantVirement(versement.getMontantVirement());
		versementDto.setCompteBeneficiaire(versement.getCompteBeneficiaire());
		versementDto.setDateExecution(versement.getDateExecution());
		versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());

		return versementDto;

	}
	
	public static List<VersementDto> listVersementToListVersementDto(List<Versement> versements){
        if (versements.isEmpty()) {
            return null;
        }
        List<VersementDto> versementDtos=new ArrayList<VersementDto>();
        
        versements.forEach(versement -> {
            versementDtos.add(versementToVersementDto(versement));
        });

        return versementDtos;
    }

}
