package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

public class CompteMapper {
	
	public static CompteDto compteToCompteDto(Compte compte) {
    	CompteDto  compteDto = new CompteDto();
        compteDto.setNrCompte(compte.getNrCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());
        return compteDto;
  }
	
	public static List<CompteDto> listCompteToListCompteDto(List<Compte> comptes){
        if (comptes.isEmpty()) {
            return null;
        }
        List<CompteDto> compteDtos=new ArrayList<>();
        
        comptes.forEach(compte-> {
        	compteDtos.add(compteToCompteDto(compte));
        });

        return compteDtos;
    }

}
