package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    private static VirementDto virementDto;

    public static VirementDto virementToVirementDto(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDateExecution(virement.getDateExecution());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setMotif(virement.getMotifVirement());       

        return virementDto;

    }
    
    
    public static List<VirementDto> listVirementToListVirementDto(List<Virement> virements){
        if (virements.isEmpty()) {
            return null;
        }
        List<VirementDto> virementDtos=new ArrayList<VirementDto>();
        
        virements.forEach(virement -> {
            virementDtos.add(virementToVirementDto(virement));
        });

        return virementDtos;
    }
}
