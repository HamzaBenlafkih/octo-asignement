package ma.octo.assignement.dto;

import java.math.BigDecimal;


public class CompteDto {
	
	private String nrCompte;
	private String rib;
	private BigDecimal solde;


	public BigDecimal getSolde() {
		return solde;
	}

	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}	
	
	public String getNrCompte() {
		return nrCompte;
	}

	public void setNrCompte(String nrCompte) {
		this.nrCompte = nrCompte;
	}

}
