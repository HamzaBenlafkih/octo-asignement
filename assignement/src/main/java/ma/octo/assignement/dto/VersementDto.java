package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ma.octo.assignement.domain.Compte;

public class VersementDto {
	
	public BigDecimal getMontantVirement() {
		return montantVirement;
	}

	public void setMontantVirement(BigDecimal montantVirement) {
		this.montantVirement = montantVirement;
	}

	public Date getDateExecution() {
		return dateExecution;
	}

	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	public String getNom_prenom_emetteur() {
		return nom_prenom_emetteur;
	}

	public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
		this.nom_prenom_emetteur = nom_prenom_emetteur;
	}

	public Compte getCompteBeneficiaire() {
		return compteBeneficiaire;
	}

	public void setCompteBeneficiaire(Compte compteBeneficiaire) {
		this.compteBeneficiaire = compteBeneficiaire;
	}

	public String getMotifVersement() {
		return motifVersement;
	}

	public void setMotifVersement(String motifVersement) {
		this.motifVersement = motifVersement;
	}

	private BigDecimal montantVirement;

	private Date dateExecution;

	private String nom_prenom_emetteur;

	private Compte compteBeneficiaire;

	private String motifVersement;
	
	 public String getRibBeneficiaire() {
		return ribBeneficiaire;
	}

	public void setRibBeneficiaire(String ribBeneficiaire) {
		this.ribBeneficiaire = ribBeneficiaire;
	}

	private String ribBeneficiaire;

}
