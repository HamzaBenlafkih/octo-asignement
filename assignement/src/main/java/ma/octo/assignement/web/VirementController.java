package ma.octo.assignement.web;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VirrementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
class VirementController {

	@Autowired
    private VirrementService virementService;
   
    
    @GetMapping(value = "/virements")
    List<VirementDto> loadAll() {
        List<VirementDto> all = virementService.AllVirement();
        return all;
    }

    

    @PostMapping("/addVirement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(@RequestBody VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
    	virementService.MakeVirement(virementDto);
    }
}
